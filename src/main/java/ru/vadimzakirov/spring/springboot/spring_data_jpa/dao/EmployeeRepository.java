package ru.vadimzakirov.spring.springboot.spring_data_jpa.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.vadimzakirov.spring.springboot.spring_data_jpa.entity.Employee;


public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}
