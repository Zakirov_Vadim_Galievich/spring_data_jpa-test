package ru.vadimzakirov.spring.springboot.spring_data_jpa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vadimzakirov.spring.springboot.spring_data_jpa.dao.EmployeeRepository;
import ru.vadimzakirov.spring.springboot.spring_data_jpa.entity.Employee;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository repository;

    @Override
    public List<Employee> getAllEmployees() {
        return repository.findAll();
    }

    @Override
    public void saveEmployee(Employee employee) {
        repository.save(employee);
    }

    @Override
    public Employee getEmployee(int id) {
        Employee employee = null;
        Optional<Employee> empOptional = repository.findById(id);
        if(empOptional.isPresent()) {
            employee = empOptional.get();
        }
            return employee;
        //нужно выбросить исключение и записать в лог

    }

    @Override
    public void deleteEmployee(int id) {
        repository.deleteById(id);
    }

}
